#
# This file contains an example of the necessary vcl to make varnish_auth work.
# NOTE use this as a guide,
#
# An example for a vcl file configured for Drupal can be found here: https://fourkitchens.atlassian.net/wiki/display/TECH/Configure+Varnish+3+for+Drupal+7

sub vcl_recv {
  # Do not cache these paths.
  if (req.url ~ "^.*\.php*" ||
      req.url ~ "^/ooyala/ping$" ||
      req.url ~ "^/admin" ||
      req.url ~ "^/admin/.*$" ||
      req.url ~ "^/user" ||
      req.url ~ "^/user/.*$" ||
      req.url ~ "^/users/.*$" ||
      req.url ~ "^/info/.*$" ||
      req.url ~ "^/flag/.*$" ||
      req.url ~ "^.*/ajax/.*$" ||
      req.url ~ "^.*/ahah/.*$" ||
      req.url ~ "^.*/node/.*/edit.*$" ||
      req.url ~ "^.*/esi/.*$") {
    return (pass);
  }

  // Check for NO_CACHE cookie,
  // this should take prevalence over authenticated session cookies
  // And general session cookies
  if (req.http.Cookie ~ "^.*NO_CACHE.*$" || req.url ~ "^.*NO_CACHE.*$") {
    return (pass);
  }

  // Authenticated cached requests
  // Check for...
  // 1) We are on the correct domain
  // 2) It's a GET or HEAD request
  // 3) We're not on a restart
  // 4) They have the USE_VARNISH_AUTH cookie
  if (req.http.host ~ "^.*yourdomain.com$"
    && (req.request == "GET" || req.request == "HEAD")
    && req.restarts == 0
    && req.http.Cookie ~ "^.*USE_VARNISH_AUTH.*$"
    ) {
      // Unset a varnish auth header (in case for some reason it was sent by the client).
      unset req.http.X-Varnish-Auth;
      // Rewrite the request with a check
      set req.http.X-Original-URL = req.url;
      set req.url = "/sites/all/modules/contrib/varnish_auth/access_check.php";
  }

  # Remove all cookies that Drupal doesn't need to know about. We explicitly
  # list the ones that Drupal does need, the SESS and NO_CACHE. If, after
  # running this code we find that either of these two cookies remains, we
  # will pass as the page cannot be cached.
  if (req.http.Cookie) {
    # 1. Append a semi-colon to the front of the cookie string.
    # 2. Remove all spaces that appear after semi-colons.
    # 3. Match the cookies we want to keep, adding the space we removed
    #    previously back. (\1) is first matching group in the regsuball.
    # 4. Remove all other cookies, identifying them by the fact that they have
    #    no space after the preceding semi-colon.
    # 5. Remove all spaces and semi-colons from the beginning and end of the
    #    cookie string.
    set req.http.Cookie = ";" + req.http.Cookie;
    set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
    set req.http.Cookie = regsuball(req.http.Cookie, ";(SESS[a-z0-9]+|SSESS[a-z0-9])=", "; \1=");
    set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

    if (req.http.Cookie == "") {
      # If there are no remaining cookies, remove the cookie header. If there
      # aren't any cookie headers, Varnish's default behavior will be to cache
      # the page.
      unset req.http.Cookie;
    }
    else {
      # If there is any cookies left (a session or NO_CACHE cookie), do not
      # cache the page. Pass it on to Apache directly.
      return (pass);
    }
  }
}

sub vcl_deliver {
  if (obj.hits > 0) {
    set resp.http.X-Varnish-Cache = "HIT";
  }
  else {
    set resp.http.X-Varnish-Cache = "MISS";
  }

 // If the response contains X-Original-URL and X-Varnish-Auth header
 if (req.http.X-Original-URL) {
   set req.url = req.http.X-Original-URL;
   set req.http.X-Varnish-Auth = resp.http.X-Varnish-Auth;
   if (resp.http.X-Varnish-Auth == "1") {
     // Unset the cookie
     unset req.http.Cookie;
   }
   unset req.http.X-Original-URL;
   return (restart);
 }
}

# Code determining what to do when serving items from the Apache servers.
sub vcl_fetch {
  if (beresp.http.esi-enabled == "1") {
    set beresp.do_esi = true;
    unset beresp.http.esi-enabled;
  }
}
