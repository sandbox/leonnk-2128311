<?php

# This file is used to authenticate users before they are served a page from Varnish cache.
# It is called from Varnish VCL, which rewrites the incoming url to this file
# We do a DRUPAL_BOOTSTRAP_SESSION to check the user is valid, and return a X-Varnish-Auth header.
# We then restart the request in vcl_deliver, with the original url and the X-Varnish-Auth header.
# There is code in varnish_auth_boot that detects the header and loads in the correct roles for the user.

//change working directory to Drupal root directory
chdir($_SERVER['DOCUMENT_ROOT'] . '/');
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

// Continue Drupal bootstrap. Establish database connection and validate session.
drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION, TRUE);

if ($user && $user->uid) {
  header('X-Varnish-Auth: 1');
}
else {
  header('X-Varnish-Auth: 0');
}

die;