Varnish Auth
============

This module provides functionality for serving pages for authenticated users from Varnish Cache.

It works by making a very-quick DRUPAL_BOOTSTRAP_SESSION, which checks the user is authenticated, and then serves a page from Varnish Cache.  This is done through VCL.

You should use this module if have a role on your site where everyone sees the same content.
You should be aware that when they are authenticated through this module, they are still the anonymous user (just modified to have the correct roles).
So anything dynamic on the page or requiring a uid will not work.
You can use ESI to include these elements.

There are several things you need to do to set this module up.

1) Install Varnish (obviously!)
2) Install the varnish Drupal module, this allows for Drupal to use Varnish for it's caching system.
3) Make Drupal cache pages for anonymous users.
4) Install the Varnish Auth module.
5) Edit your VCL to use code provided in example.vcl
   IMPORTANT: ensure you edit lines 38 and 47 of example.vcl with your own details.
6) Add the following to your settings.php

$conf['varnish_auth_roles'] = array(4);
$conf['varnish_auth_domains'] = array(2);

These variables determine which role(s) and domain access domain(s) should be used for authenticated caching.  Domain is optional.
Varnish Auth works by matching ALL roles and domains, the user needs to be assigned exactly the same as your settings, no more no less.
varnish_auth_exclude_forms says which forms to exclude (for details see below).

How Does This Module Work?
==========================
Below is a step by step run through of what happens during a varnish_auth request

1) The user logs in, varnish_auth checks they have the same roles and domain as our settings, and sets a USE_VARNISH_AUTH cookie
2) The user then requests another page, vcl checks if the USE_VARNISH_AUTH exists (this is in the vcl_rec function)
3) If so, it rewrites the url to the access_check.php file, copying over the original url as a header (to be used later).
   The request is passed directly to Apache (not cached).
4) The access_check.php file does a lightweight DRUPAL_BOOTSTRAP_SESSION to check the user is logged in, if so sets a X-Varnish-Auth
5) Vcl checks the results of this requests in vcl_deliver.
   If the X-Varnish-Auth header has been set to 1 it restarts the process with the original url and stripped out cookies.
6) This second requests goes straight through to the Varnish cache, we are now considered anonymous but we have the X-Varnish-Auth header
7) hook_boot and hook_user_load check if this header exists, and if so modify the user object with the roles and domains from our settings

